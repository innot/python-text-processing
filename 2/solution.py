#!/usr/bin/env python2
#!coding: utf-8

import cPickle
import nltk
import re
import string
import codecs
import collections
import itertools
import sklearn
from sklearn import cross_validation
from nltk.corpus import brown
from nltk.tbl.template import Template
from nltk.tag.brill import Pos, Word
from nltk.tag import RegexpTagger, BrillTaggerTrainer

class Solution(nltk.tag.api.TaggerI):
    def __init__(self, data, debug = 2):
        patterns = [
                (r'.*\xf2\xfc$', 'V'),
                (r'.*\xf1\xff$', 'V'),
                #(r'.*[^\xf2]\xfc$', 'S'),
                (r'.*\xfb\xe9$', 'A'),
                (r'.*\xee\xe5$', 'A'),
                (r'.*[\xed]{1,2}\xee$', 'ADV'),
                #(r'\xef\xee-.*(\xee\xec\xf3|\xe5\xec\xf3|\xe8)$', 'ADV'),
                (r'.*(\xf3|\xfe)\xf9\xe5\xec\xf3(\xf1\xff)?$', 'A'),
                ]
        data = self._data_to_utf(data)
        t0 = nltk.DefaultTagger('S')
        t1 = nltk.RegexpTagger(patterns, backoff = t0)
        t2 = nltk.UnigramTagger(data, backoff = t1)
        t3 = nltk.BigramTagger(data, backoff = t2)
        #t4 = nltk.ClassifierBasedTagger(feature_detector, train = data, backoff = t3, cutoff_prob = 0.3)
        #t4 = nltk.NgramTagger(3, data, backoff = t3)

        # Template._cleartemplates()
        # templates = [Template(Pos([-1])), Template(Pos([-1]), Word([0]))]
        # tt = BrillTaggerTrainer(t3, templates, trace = 3)
        # t4 = tt.train(data, max_rules = 10)
        self.tagger = t3

    def tag(self, input):
        x = self.tagger.tag(self._sentence_to_utf(input))
        # print x
        return x

    def _sentence_to_utf(self, sentence):
        return sentence
        #return map(lambda (t): unicode(t, 'cp1251'), sentence)

    def _data_to_utf(self, data):
        return data
        #return map(lambda (i): map(lambda (j): tuple([unicode(j[0], 'cp1251').lower().encode('cp1251'), j[1]]), i), data)

if __name__ == '__main__':
    data = cPickle.load(open('train.pkl'))
    x = int(0.8 * len(data))
    sol = Solution(data[:x])
    # data = sol._data_to_utf(data)
    print sol.evaluate(data[x:])
